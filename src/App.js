import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Main from "./component/Main/Main";
import "bootstrap/dist/css/bootstrap.min.css";
import Signup from "./component/screens/signup/Signup";
import Signin from "./component/screens/signin/Signin";

function App() {
  return (
    <BrowserRouter>
      <Route path="/" exact={true} component={Main} />
      <Route path="/signup" component={Signup} />
      <Route path="/signin" component={Signin} />
    </BrowserRouter>
  );
}

export default App;
