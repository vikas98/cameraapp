import { ArrowBack, ArrowForward } from "@material-ui/icons";
import React from "react";
import {
  Apps,
  Camera,
  CardMembership,
  Notifications,
  Person,
  Share,
  Videocam,
} from "@material-ui/icons";
import "./sidebar.css";
import Avatar from "react-avatar";

function Sidebar({
  home,
  account,
  viewAll,
  device,
  activity,
  share,
  subscription,
  homeFunction,
  cameraFunction,
  activityFunction,
  viewAllFunction,
  subscriptionFunction,
  shareFunction,
  accountFunction,
  collapsed,
  collapsedFunction,
}) {
  return (
    <>
      {collapsed ? (
        <>
          <div className="personal-info-collapsed">
            <div className="personal-avatar-collapsed">
              <div>
                <ArrowForward onClick={() => collapsedFunction(false)} />{" "}
              </div>
              <Avatar size="30" round={true} name="akhil" />
            </div>
          </div>
          <div className="sidebar-items-collapsed">
            {home ? (
              <div
                className="sidebar-links-collapsed"
                style={{ color: "black" }}
              >
                <Videocam />
              </div>
            ) : (
              <div
                className="sidebar-links-collapsed"
                onClick={homeFunction}
                style={{ color: " #C0C0C0" }}
              >
                <Videocam />
              </div>
            )}
            {device ? (
              <div className="sidebar-links-collapsed">
                <Camera />
              </div>
            ) : (
              <div
                className="sidebar-links-collapsed"
                onClick={cameraFunction}
                style={{ color: " #C0C0C0" }}
              >
                <Camera />
              </div>
            )}
            {activity ? (
              <div className="sidebar-links-collapsed">
                <Notifications />
              </div>
            ) : (
              <div
                className="sidebar-links-collapsed"
                onClick={activityFunction}
                style={{ color: " #C0C0C0" }}
              >
                <Notifications />
              </div>
            )}
            {viewAll ? (
              <div className="sidebar-links-collapsed">
                <Apps />
              </div>
            ) : (
              <div
                className="sidebar-links-collapsed"
                onClick={viewAllFunction}
                style={{ color: " #C0C0C0" }}
              >
                <Apps />
              </div>
            )}
            {account ? (
              <div className="sidebar-links-collapsed">
                <Person />
              </div>
            ) : (
              <div
                className="sidebar-links-collapsed"
                onClick={accountFunction}
                style={{ color: " #C0C0C0" }}
              >
                <Person />
              </div>
            )}
            {subscription ? (
              <div className="sidebar-links-collapsed">
                <CardMembership />
              </div>
            ) : (
              <div
                className="sidebar-links-collapsed"
                onClick={subscriptionFunction}
                style={{ color: " #C0C0C0" }}
              >
                <CardMembership />
              </div>
            )}
            {share ? (
              <div className="sidebar-links-collapsed">
                <Share />
              </div>
            ) : (
              <div
                className="sidebar-links-collapsed"
                onClick={shareFunction}
                style={{ color: "#C0C0C0" }}
              >
                <Share />
              </div>
            )}
          </div>
        </>
      ) : (
        <>
          <div className="personal-info">
            {/* <div>
              <ArrowBack onClick={() => collapsedFunction(true)} />{" "}
            </div> */}
            <div className="personal-avatar">
              <Avatar size="50" round={true} name="akhil" />
            </div>
            <div className="personal-data">
              <div>
                <h5 style={{ marginBottom: "0" }}>Akhil Jagga</h5>
              </div>
              <div className="text-muted">akhil@mail.com</div>
            </div>
          </div>
          <div className="sidebar-items">
            {home ? (
              <div className="sidebar-links" style={{ color: "black" }}>
                <Videocam /> &nbsp; Home
              </div>
            ) : (
              <div
                className="sidebar-links"
                onClick={homeFunction}
                style={{ color: " #C0C0C0" }}
              >
                <Videocam />
                &nbsp; Home
              </div>
            )}
            {device ? (
              <div className="sidebar-links">
                <Camera />
                &nbsp; Device's
              </div>
            ) : (
              <div
                className="sidebar-links"
                onClick={cameraFunction}
                style={{ color: " #C0C0C0" }}
              >
                <Camera />
                &nbsp; Device's
              </div>
            )}
            {activity ? (
              <div className="sidebar-links">
                <Notifications />
                &nbsp; Last Activity
              </div>
            ) : (
              <div
                className="sidebar-links"
                onClick={activityFunction}
                style={{ color: " #C0C0C0" }}
              >
                <Notifications />
                &nbsp; Last Activity
              </div>
            )}
            {viewAll ? (
              <div className="sidebar-links">
                <Apps />
                &nbsp; View All
              </div>
            ) : (
              <div
                className="sidebar-links"
                onClick={viewAllFunction}
                style={{ color: " #C0C0C0" }}
              >
                <Apps />
                &nbsp; View All
              </div>
            )}
            {account ? (
              <div className="sidebar-links">
                <Person />
                &nbsp; Account
              </div>
            ) : (
              <div
                className="sidebar-links"
                onClick={accountFunction}
                style={{ color: " #C0C0C0" }}
              >
                <Person />
                &nbsp; Account
              </div>
            )}
            {subscription ? (
              <div className="sidebar-links">
                <CardMembership />
                &nbsp; Subscriptions
              </div>
            ) : (
              <div
                className="sidebar-links"
                onClick={subscriptionFunction}
                style={{ color: " #C0C0C0" }}
              >
                <CardMembership />
                &nbsp; Subscriptions
              </div>
            )}
            {share ? (
              <div className="sidebar-links">
                <Share />
                &nbsp; Share
              </div>
            ) : (
              <div
                className="sidebar-links"
                onClick={shareFunction}
                style={{ color: "#C0C0C0" }}
              >
                <Share />
                &nbsp; Share
              </div>
            )}
          </div>
        </>
      )}
    </>
  );
}

export default Sidebar;
