import React from "react";
import "./loader.css";
import { Col, Container, Row } from "react-bootstrap";
import Load from "../../assets/images/Groupload.png";

function Loader() {
  return (
    <div>
      <Container>
        <Row className="justify-content-md-center">
          <Col md="auto">
            <img className="main-image" src={Load} alt="loading......" />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Loader;
