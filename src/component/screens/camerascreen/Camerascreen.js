import { ChevronLeftOutlined, ChevronRightOutlined, FullscreenOutlined, PlayCircleFilledOutlined, Share } from "@material-ui/icons";
import React from "react";
import { Col, Row } from "react-bootstrap";
import houseentrance from "../../../assets/images/house entrance.png";
import "./Camerascreen.css";
import WifiIcon from "@material-ui/icons/Wifi";
import Battery60Icon from "@material-ui/icons/Battery60";
function Camerascreen() {
  return (
    <div>
      <Row>
        <Col lg={7}>
          <div style={{ position: "relative" }}>
            <h3 className="div-house">House Entrance</h3>
            <div className="house-tag">
              <div className="house-ww">House entrance</div>
              <div><Battery60Icon />
                <WifiIcon />
            
              </div>
            </div>
            <img
              style={{ width: "608px", margin: "4%" }}
              src={houseentrance}
              alt="houseentrancehouseentrance"
            ></img>
            <div className="button-image">
              <Share />
              <div>
                <ChevronLeftOutlined />
                <PlayCircleFilledOutlined />
                <ChevronRightOutlined />
              </div>
              <FullscreenOutlined />
            </div>
          </div>
        </Col>
        <Col lg={5}>
          <h5 className="last-tag">Last activity</h5>

          <div className="col-images">
            <img
              className="col-image"
              style={{ width: "411px", verticalAlign: "baseline" }}
              src={houseentrance}
              alt="houseentrance"
            ></img>
            <p className="p-time">Time realesed: 01:10 PM</p>
            <p className="pa-t">Notification: People was detected</p>
          </div>
          <div>
            <img
              className="col-image"
              style={{ width: "411px", verticalAlign: "baseline" }}
              src={houseentrance}
              alt="houseentrance"
            ></img>
            <p className="p-time">Time realesed: 01:10 PM</p>
            <p className="pa-t">Notification: People was detected</p>
          </div>
          <div>
            <img
              className="col-image"
              style={{ width: "411px", verticalAlign: "baseline" }}
              src={houseentrance}
              alt="houseentrance"
            ></img>
            <p className="p-time">Time realesed: 01:10 PM</p>
            <p className="pa-t">Notification: People was detected</p>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default Camerascreen;
