import React, { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { CopyToClipboard } from "react-copy-to-clipboard";
import "./share.css";

function Share({ setShareFunction }) {
  const [email, setEmail] = useState("");
  const [show, setShow] = useState(true);
  const [copy, setCopy] = useState(false);
  const copyText =
    " https://www.google.com/search?q=mobile+sharing+browser+ui&rlz=1C5CHFA_enUA921UA921&sxsrf";

  const handleClose = () => {
    setShareFunction();
    setShow(false);
  };
  const handleShow = () => {
    setShow(true);
  };
  return (
    show && (
      <Modal className="share-modal" show={show} onHide={handleClose}>
        <div className=" text-center share">
          <div className="share-main">
            <div>
              <h3>Share the app</h3>
              <a href="#">
                https://www.google.com/search?q=mobile+sharing+browser+ui&rlz=1C5CHFA_enUA921UA921&sxsrf
              </a>
            </div>
            <div className="share-close-button">
              <Button
                style={{ borderRadius: "100%", fontWeight: "bolder" }}
                variant="danger"
                onClick={handleClose}
              >
                X
              </Button>
            </div>
          </div>
          <div className="share-input">
            <div style={{ display: "flex", justifyContent: "center" }}>
              <input
                type="text"
                placeholder="enter email address"
                className="input-tag-share"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              ></input>
              &nbsp; &nbsp;
              <Button variant="dark">Share Link</Button>
            </div>
            <div>
              <p> or </p>
              <CopyToClipboard
                text={copyText}
                onCopy={() => {
                  setCopy(true);
                  setTimeout(function () {
                    setCopy(false);
                  }, 3000);
                }}
              >
                <span
                  style={{
                    color: "blue",
                    textDecoration: "underline",
                    cursor: "pointer",
                  }}
                >
                  Copy Link
                </span>
              </CopyToClipboard>
              &nbsp; &nbsp; &nbsp;
              {copy && <spam style={{ color: "red" }}>Copied!</spam>}
            </div>
          </div>
        </div>
      </Modal>
    )
  );
}

export default Share;
