import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import "./accountComponent.css";

function SetEmail() {
  const [email, setEmail] = useState("");
  useEffect(() => {
    setEmail("akhil@mail.com"); 																		
  }, []);
  return (
    <div>
      <div>
        <h4 className="email-component-h4">Change Email</h4>
        <div className="email-component-input">
          <p className="text-muted">Email</p>
          <input
            type="text"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="email-component-button">
          <Button variant="primary">Save</Button>
        </div>
      </div>
    </div>
  );
}

export default SetEmail;
