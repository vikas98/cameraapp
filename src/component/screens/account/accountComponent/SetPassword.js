import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import "./accountComponent.css";

function SetPassword() {
  const [password, setPassword] = useState("Haha");
  const [newPassword, setNewPassword] = useState("");
  const [reTypePassword, setReTypePassword] = useState("");

  const saveButtonClicked = () => {
    if (newPassword !== reTypePassword) {
      alert("passsword does not match");
    }
  };
  useEffect(() => {
    return () => {};
  }, []);

  return (
    <div>
      <h4 className="password-component-h4">Change Password</h4>
      <div className="password-component-input">
        <div>
          <p className="text-muted">old password</p>
          <input type="password" value={password} />
        </div>
        <div>
          <p className="text-muted">New Password</p>
          <input
            type="password"
            value={newPassword}
            onChange={(e) => setNewPassword(e.target.value)}
          />
        </div>
        <div>
          <p className="text-muted">Confirm New Password</p>
          <input
            type="password"
            value={reTypePassword}
            onChange={(e) => setReTypePassword(e.target.value)}
          />
        </div>
      </div>
      <div className="name-component-button">
        <Button onClick={saveButtonClicked} variant="primary">
          Save
        </Button>
      </div>
    </div>
  );
}

export default SetPassword;
