import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import "./accountComponent.css";

function SetName() {
  const [name, setName] = useState("");
  useEffect(() => {
    setName("Akhil Jagga");
    return () => {};
  }, []);
  return (
    <div>
      <h4 className="name-component-h4">Change Name</h4>
      <div className="name-component-input">
        <p className="text-muted">Name</p>
        <input
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </div>
      <div className="name-component-button">
        <Button variant="primary">Save</Button>
      </div>
    </div>
  );
}

export default SetName;
