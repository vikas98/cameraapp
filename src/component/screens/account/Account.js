import React, { useState } from "react";
import Avatar from "react-avatar";
import { Button, Card, Col, Form, Modal, Row } from "react-bootstrap";
import "./account.css";
import ImageAvatar from "../../../assets/images/Ovalimage.png";
import { ArrowRight } from "@material-ui/icons";
import SetName from "./accountComponent/SetName";
import SetEmail from "./accountComponent/SetEmail";
import SetPassword from "./accountComponent/SetPassword";
import OtpInput from "react-otp-input";

function Account() {
  const [name, setName] = useState("Akhil Jagga");
  const [email, setEmail] = useState("akhil@mail.com");
  const [password, setPassword] = useState("lol");
  const [showCOl2, setCol2] = useState(false);
  const [showName, setShowName] = useState(false);
  const [showEmail, setShowEmail] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [show, setShow] = useState(false);
  const [otp, setOtp] = useState("");

  const handleClose = () => setShow(false);
  const handleShow = () => {
    setShow(true);
    setCol2(false);
  };

  const nameClicked = () => {
    setCol2(true);
    setShowName(true);
    setShowEmail(false);
    setShowPassword(false);
  };
  const emailClicked = () => {
    setCol2(true);
    setShowName(false);
    setShowEmail(true);
    setShowPassword(false);
  };
  const passwordClicked = () => {
    setCol2(true);
    setShowName(false);
    setShowEmail(false);
    setShowPassword(true);
  };
  return (
    <div className="account">
      <h5 style={{ color: "white", padding: "4% 4% 1% 4%" }}>Account</h5>
      <Row>
        <Col lg={4}>
          <Card className="account-card-1" style={{ marginLeft: "4%" }}>
            <div className="account-info">
              <Avatar
                name="Akhil Jagga"
                src={ImageAvatar}
                round={true}
                size="100"
              />
              <h4 style={{ marginBottom: "0", marginTop: "10px" }}>
                Akhil Jagga
              </h4>
              <p className="text-muted">akhil@mail.com</p>
            </div>
            <Card.Body style={{ padding: "0" }}>
              <div style={{}}>
                <div
                  style={{
                    padding: "0 20px 0 20px",
                  }}
                  className="account-update"
                >
                  <p className="text-muted">Name</p>
                  <h6 onClick={nameClicked}>Akhil Jagga</h6>
                </div>
                <div
                  style={{ padding: "0 20px 0 20px" }}
                  className="account-update"
                >
                  <p className="text-muted">Email</p>
                  <h6 onClick={emailClicked}>akhil@mail.com</h6>
                </div>
                <div
                  style={{ padding: "0 20px 0 20px" }}
                  className="account-update"
                >
                  <h6
                    onClick={passwordClicked}
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <>Change Password</> <ArrowRight />{" "}
                  </h6>
                </div>
              </div>
              <br />
              <div className="empty"></div>
              <div style={{ padding: "20px 20px 0 20px" }}>
                <div className="account-options">
                  <h6
                    onClick={handleShow}
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <>Change Pin Code</> <ArrowRight />{" "}
                  </h6>
                </div>
                <div className="account-options">
                  <h6
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <>Support Logout</> <ArrowRight />{" "}
                  </h6>
                </div>
                <div className="account-options">
                  <h6
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <>Logout</> <ArrowRight />{" "}
                  </h6>
                </div>
              </div>
            </Card.Body>
          </Card>
        </Col>
        {showCOl2 && (
          <Col lg={8}>
            <Card className="account-card-2">
              {showName && <SetName />}
              {showEmail && <SetEmail />}
              {showPassword && <SetPassword />}
            </Card>
          </Col>
        )}
      </Row>

      <Modal style={{ marginTop: "4rem" }} show={show} onHide={handleClose}>
        <Modal.Header
          className="account-modal-header"
          closeButton
        ></Modal.Header>
        <Modal.Body>
          <OtpInput
            value={otp}
            onChange={(e) => setOtp(e)}
            numInputs={4}
            separator={<span> &nbsp; &nbsp; </span>}
          />
          <Form.Text className="text-muted">please type your pin</Form.Text>
          <Button
            style={{ width: "30%", marginTop: "2rem" }}
            className="button"
            variant="primary"
          >
            Submit
          </Button>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default Account;
