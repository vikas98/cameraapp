import React, { useEffect, useState } from "react";
import {
  Button,
  ButtonGroup,
  Form,
  FormControl,
  Modal,
  Nav,
  Navbar,
  NavDropdown,
  Spinner,
} from "react-bootstrap";
import AppsIcon from "@material-ui/icons/Apps";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import kitchen from "../../../assets/images/Group 5 Copykitchenhoom (1).png";

import ViewListIcon from "@material-ui/icons/ViewList";
import { DirectionsCar, Email, Home } from "@material-ui/icons";
import "./Homescreen.css";
import Avatar from "react-avatar";

function Homescreen() {
  const [carColor, setCarColor] = useState("none");
  const [ShowModal, setShowModal] = useState(false);
  const [ShowModal1, setShowModal1] = useState(false);
  const [homeColor, setHomeColor] = useState("#1e82d2");
  const [showmodal2, setShowmodal2] = useState(false);
  const [Addnew, SetAddnew] = useState([]);
  const [addModal, setAddModal] = useState(false);
  const [error, setError] = useState(true);
  const handleClose = () => setShowModal(false);
  const handleShow = () => setShowModal(true);
  const handleclose1 = () => setShowModal1(false);
  const handleshow1 = () => setShowModal1(true);
  const [email, setEmail] = useState("");
  const [Name, setName] = useState("");
  const handleclose2 = () => setShowmodal2(false);
  const handleshow2 = () => setShowmodal2(true);

  const handleClose3 = () => setAddModal(false);
  const handleShow3 = () => setAddModal(true);

  const Addnewfunction = () => {
    handleShow3();
    handleclose2();
    
  };
   const addshareusserfunction=(e)=>{
     e.preventDefault()
SetAddnew((old) => {
  return [...old, {
  userName:Name,
  userEmail:email,
  }];
});
handleClose3()
handleshow2()
setName("")
setEmail("")
   }

  const carClicked = () => {
    setCarColor("#1e82d2");
    setHomeColor("");
  };
  const homeClicked = () => {
    setHomeColor("#1e82d2");
    setCarColor("");
  };
  let styleCarVariable = {
    backgroundColor: `${carColor}`,
  };
  let styleHomeVariable = {
    backgroundColor: `${homeColor}`,
  };
  const addFunction = () => {
    //   error ?   handleShow(),
    //   setTimeout(function(){handleshow1()},3000) :
    //   handleshow1()
    // }
    if (error) {
      handleclose1();
      handleClose();
      handleShow();
      setTimeout(function () {
        handleClose();
        handleshow1();
      }, 3000);
    } else {
      handleClose();
      handleshow1();
    }
  };
  useEffect(() => {
    handleshow2();
  }, []);
  return (
    <div>
      <div>
        <div>
          <Navbar expand="lg">
            <div className="AddcircleIcon">
              <AddCircleIcon onClick={addFunction}> </AddCircleIcon>
            </div>
            <div className="text-addcircleicon"> My camera (home)</div>
            <div>
              <Modal show={ShowModal} onHide={handleClose}>
                <Modal.Header closeButton></Modal.Header>
                <Modal.Body>
                  connection!
                  <p>please wait,searching for active camera</p>
                  <Spinner animation="border" role="status">
                    <span className="sr-only">Loading...</span>
                  </Spinner>
                </Modal.Body>
              </Modal>
            </div>
            <div>
              <Modal show={ShowModal1} onHide={handleclose1}>
                <Modal.Header closeButton></Modal.Header>
                <Modal.Body>
                  Device not found !!
                  <p>
                    we couldn`t find a new device near you, please check the
                    internet connection on all your devices
                  </p>
                  <Button onClick={addFunction}>try again</Button>
                </Modal.Body>
              </Modal>
            </div>
            <div>
              <Modal
                className="modal-homescreen-add"
                show={showmodal2}
                onHide={handleclose2}
              >
                <div className="modal-div">
                  <p className="share-tag">share camera video </p>
                  <p className="paharagraph-tag">
                    you can share this videos with your friends
                  </p>
                </div>
                <Modal.Header closeButton></Modal.Header>
                <Modal.Body className="modal-cantain-div" >
                  <div className="modal-body-div">
                    <div className="avatr-div">
                      <div className="avatar-div-one">
                        <Avatar
                          round={true}
                          size="70"
                          className="avatar-one"
                          name="Luis Calvillo"
                        ></Avatar>
                        <div className="modal-text">
                          <h6>Luis Calvillo</h6>
                          <p>luiscalvillo@gmail.com</p>
                        </div>
                      </div>
                      <div className="avatar-div-one">
                        <Avatar
                          round={true}
                          size="70"
                          name="Sukmeet gorae"
                          className="avatar-one"
                        ></Avatar>

                        <div className="modal-text">
                          <h6>Sukmeet gorae</h6>
                          <p> sukhmeetgorae@gmail.com</p>
                        </div>
                      </div>
                      {Addnew.map((e, i) => {
                        return (
                          <div className="avatar-div-one">
                            <Avatar
                              round={true}
                              size="70"
                              name={e.userName}
                              className="avatar-one"
                            ></Avatar>

                            <div className="modal-text">
                              <h6>{e.userName}</h6>
                              <p> {e.userEmail}</p>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </Modal.Body>
                <Modal.Footer className="footer-homescreen">
                  <div className="avatar-div-one">
                    <Avatar
                      onClick={Addnewfunction}
                      round={true}
                      size="70"
                      name="+"
                      className="avatar-one"
                    ></Avatar>

                    <div className="modal-text">
                      <h6>Add New</h6>
                      <p> example@gmail.com</p>
                    </div>
                  </div>
                  <br></br>
                </Modal.Footer>
                <Button className="button-close" onClick={handleclose2}>
                  share
                </Button>
              </Modal>
            </div>

            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ml-auto mr-auto">
                <Nav.Link href="#home">
                  <div className="signup-icons">
                    <div className="signup-icons-div">
                      <div
                        onClick={homeClicked}
                        style={styleHomeVariable}
                        className="signup-icon-home"
                      >
                        <Home className="icon" />
                      </div>
                      <div
                        onClick={carClicked}
                        style={styleCarVariable}
                        className="signup-icon-car"
                      >
                        <DirectionsCar className="icon" />
                      </div>
                    </div>
                  </div>
                </Nav.Link>
                <Nav.Link href="#link"></Nav.Link>
              </Nav>
              <Form inline>
                <ViewListIcon></ViewListIcon>
                <AppsIcon></AppsIcon>
              </Form>
            </Navbar.Collapse>
          </Navbar>
        </div>

        <div className="hover-kitchen">
          <img className="kithenhoom" src={kitchen} alt={kitchen}></img>

          <img className="kithenhoom" src={kitchen} alt={kitchen}></img>
        </div>
      </div>

      <Modal show={addModal} onHide={handleClose3}>
        <Modal.Header closeButton></Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="formBasicPassword ">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="Name"
                placeholder="Name"
                value={Name}
                onChange={(e) => setName(e.target.value)}
                required
              />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label> Enter email</Form.Label>
              <Form.Control
                type="E-MAIL"
                placeholder="ENTER YOUR EMAIL"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Form.Group>

            <Button
              variant="primary"
              type="submit"
              onClick={addshareusserfunction}
            >
              Submit
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default Homescreen;
