import React from "react";
import { DirectionsCar, Home } from "@material-ui/icons";
import { Button, Col, Container, Form, Modal, Row } from "react-bootstrap";
import Background from "../../../assets/images/Bitmapsignuphouse.png";
import Load from "../../../assets/images/Grouplogo.png";
import OtpInput from "react-otp-input";
import "./signup.css";
import { useState } from "react";
import { Link } from "react-router-dom";

function Signup() {
  const [carColor, setCarColor] = useState("none");
  const [homeColor, setHomeColor] = useState("#1e82d2");
  const [buttonDissabled, setButtonDissabled] = useState(true);
  const [otp, setOtp] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const signupFormSubmitted = (e) => {
    e.preventDefault();
    handleShow();
  };
  const random = () => {
    if (name !== "") {
      if (email !== "") {
        if (password !== "") {
          setButtonDissabled(false);
        }
      }
    }
  };
  const carClicked = () => {
    setCarColor("#1e82d2");
    setHomeColor("");
  };
  const homeClicked = () => {
    setHomeColor("#1e82d2");
    setCarColor("");
  };
  var backgroundImageStyle = {
    backgroundImage: `url(${Background})`,
    backgroundRepeat: "no-repeat",
    width: "100%",
    backgroundSize: "100% 100%",
    minHeight: "100vh",
  };
  let styleCarVariable = {
    backgroundColor: `${carColor}`,
  };
  let styleHomeVariable = {
    backgroundColor: `${homeColor}`,
  };
  return (
    <Container fluid>
      <Row>
        <Col
          style={backgroundImageStyle}
          className="signup-image-side-col"
          sm={6}
          xs={0}
        >
          <div className="signup-left-side">
            <div>
              <img className="signup-logo" src={Load} alt="logo" />
            </div>
            <div className="info">
              <h4>
                <b>House Camera App</b>
              </h4>
              <p>every thing before your eyes</p>
            </div>
            <div className="detail-info">
              <p>
                Lorem ipsum dolor sit amet, consectetur and it adipiscing elit,
                sed do eiusmod tem incididunt ut labore et dolore magna enim ad
                minim veniam, quis nostrud. Lorem ipsum dolor sit amet,
                consectetur and it adipiscing elit, sed do eiusmod tem
                incididunt ut labore et dolore magna enim ad minim veniam, quis
                nostrud. Lorem ipsum dolor sit amet, consectetur and it
                adipiscing elit, sed do eiusmod tem incididunt ut labore et
                dolore magna enim ad minim veniam, quis nostrud. Lorem ipsum
                dolor sit amet, consectetur and it adipiscing elit, sed do
                eiusmod tem incididunt ut labore et dolore magna enim ad minim
                veniam, quis nostrud.
              </p>
            </div>
          </div>
        </Col>
        <Col className="signup-form-col" xs={12} sm={6}>
          <div className="signup-icons">
            <div className="signup-icons-div">
              <div
                onClick={homeClicked}
                style={styleHomeVariable}
                className="signup-icon-home"
              >
                <Home className="icon" />
              </div>
              <div
                onClick={carClicked}
                style={styleCarVariable}
                className="signup-icon-car"
              >
                <DirectionsCar className="icon" />
              </div>
            </div>
          </div>
          <div className="signup-form">
            <h1>
              <b>Signup</b>
            </h1>
            <Form onSubmit={signupFormSubmitted}>
              <Form.Group>
                <Form.Control
                  type="text"
                  value={name}
                  placeholder="Enter Name"
                  onChange={(e) => {
                    random();
                    setName(e.target.value);
                  }}
                />
              </Form.Group>
              <Form.Group controlId="formBasicEmail">
                <Form.Control
                  value={email}
                  type="email"
                  placeholder="Enter email"
                  onChange={(e) => {
                    random();
                    setEmail(e.target.value);
                  }}
                />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Control
                  value={password}
                  type="password"
                  placeholder="Password"
                  onChange={(e) => {
                    random();
                    setPassword(e.target.value);
                  }}
                />
              </Form.Group>
              {/* <Form.Group controlId="formBasicPassword">
                <Form.Control type="password" placeholder="Pin-code" />
              </Form.Group> */}
              {buttonDissabled ? (
                <Button
                  className="button"
                  variant="dark"
                  style={{ width: "100%" }}
                  type="submit"
                  disabled
                >
                  Signup
                </Button>
              ) : (
                <Button
                  className="button"
                  variant="dark"
                  style={{ width: "100%" }}
                  type="submit"
                >
                  Signup
                </Button>
              )}
            </Form>
            <div className="login-option">
              Already have an account? <Link to="/signin">login</Link>
            </div>
          </div>
        </Col>
      </Row>
      <Modal style={{ marginTop: "4rem" }} show={show} onHide={handleClose}>
        <Modal.Body>
          <OtpInput
            value={otp}
            onChange={(e) => setOtp(e)}
            numInputs={4}
            separator={<span> &nbsp; &nbsp; </span>}
          />
          <Form.Text className="text-muted">please type your pin</Form.Text>
          <Button
            style={{ width: "30%", marginTop: "2rem" }}
            className="button"
            variant="primary"
          >
            Submit
          </Button>
        </Modal.Body>
      </Modal>
    </Container>
  );
}

export default Signup;
