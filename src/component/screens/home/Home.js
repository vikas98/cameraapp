import React, { useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import Sidebar from "../../sidebar/Sidebar";
import Account from "../account/Account";
import Homescreen from "../Homescreen/Homescreen";
import LastActivity from "../lastActivity/LastActivity";
import Share from "../share/Share";
import SubscriptionScreen from "../subscription/SubscriptionScreen";
import ViewAll from "../viewAll/ViewAll";
import Camerascreen from "../camerascreen/Camerascreen";
import DEvicescreen from "../Devicescren/DEvicescreen";
import "./home.css";

function Home() {
  const [home, setHome] = useState(true);
  const [device, setDevice] = useState(false);
  const [activity, setActivity] = useState(false);
  const [subscription, setSubscription] = useState(false);
  const [share, setShare] = useState(false);
  const [viewAll, setViewAll] = useState(false);
  const [account, setAccount] = useState(false);
  const [collapsed, setCollapsed] = useState(false);

  const homeClicked = () => {
    setHome(true);
    setDevice(false);
    setAccount(false);
    setActivity(false);
    setSubscription(false);
    setShare(false);
    setViewAll(false);
  };
  const cameraClicked = () => {
    setHome(false);
    setDevice(true);
    setAccount(false);
    setActivity(false);
    setSubscription(false);
    setShare(false);
    setViewAll(false);
  };
  const accountClicked = () => {
    setHome(false);
    setDevice(false);
    setAccount(true);
    setActivity(false);
    setSubscription(false);
    setShare(false);
    setViewAll(false);
  };
  const activityClicked = () => {
    setHome(false);
    setDevice(false);
    setAccount(false);
    setActivity(true);
    setSubscription(false);
    setShare(false);
    setViewAll(false);
  };
  const subscriptionClicked = () => {
    setHome(false);
    setDevice(false);
    setAccount(false);
    setActivity(false);
    setSubscription(true);
    setShare(false);
    setViewAll(false);
  };
  const shareClicked = () => {
    setHome(false);
    setDevice(false);
    setAccount(false);
    setActivity(false);
    setSubscription(false);
    setShare(true);
    setViewAll(false);
  };
  const viewAllClicked = () => {
    setHome(false);
    setDevice(false);
    setAccount(false);
    setActivity(false);
    setSubscription(false);
    setShare(false);
    setViewAll(true);
  };
  // if (share) {
  //   alert("lol");
  //   document.querySelectorAll(".modal-content").style.width = "126%";
  // }

  return (
    <Container style={{ backgroundColor: "#EBF5FB" }} fluid>
      <Row>
        {collapsed ? (
          <Col className="sidebar-col-collapsed" lg={1} md={2} sm={2} xs={4}>
            <Sidebar
              home={home}
              account={account}
              viewAll={viewAll}
              device={device}
              activity={activity}
              share={share}
              subscription={subscription}
              collapsed={collapsed}
              homeFunction={homeClicked}
              cameraFunction={cameraClicked}
              activityFunction={activityClicked}
              viewAllFunction={viewAllClicked}
              shareFunction={shareClicked}
              subscriptionFunction={subscriptionClicked}
              accountFunction={accountClicked}
              collapsedFunction={setCollapsed}
            />
          </Col>
        ) : (
          <Col className="sidebar-col" xl={2} lg={3} md={4} sm={2} xs={4}>
            <Sidebar
              home={home}
              account={account}
              viewAll={viewAll}
              device={device}
              activity={activity}
              share={share}
              subscription={subscription}
              collapsed={collapsed}
              homeFunction={homeClicked}
              cameraFunction={cameraClicked}
              activityFunction={activityClicked}
              viewAllFunction={viewAllClicked}
              shareFunction={shareClicked}
              subscriptionFunction={subscriptionClicked}
              accountFunction={accountClicked}
              collapsedFunction={setCollapsed}
            />
          </Col>
        )}

        <Col
          fluid
          className="data-col-collapsed"
          xl={10}
          lg={9}
          md={8}
          sm={10}
          xs={8}
        >
          {home && <Homescreen />}
          {device && <DEvicescreen />}
          {viewAll && <ViewAll />}
          {subscription && <SubscriptionScreen />}
          {account && <Account />}
          {activity && <LastActivity />}
          {share && <Share setShareFunction={homeClicked} />}
        </Col>
      </Row>
    </Container>
  );
}

export default Home;
