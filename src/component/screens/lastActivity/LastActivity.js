import React, { useEffect, useState } from "react";
import Calendar from "react-calendar";
import Bedroom from "../../../assets/images/bedroom.png";
import LivingRoom from "../../../assets/images/livingRoom.png";
import Kitchen from "../../../assets/images/kitchen.png";
import Play from "../../../assets/images/videoplay.png";
import "react-calendar/dist/Calendar.css";
import "./lastActivity.css";
import { Button, Modal } from "react-bootstrap";

function LastActivity() {
  const [value, setValue] = useState(new Date());
  const [arrayRandom, setArrayRandom] = useState([]);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  setInterval(updateTime, 1000);

  const now = new Date().toLocaleTimeString();

  const [time, setTime] = useState(now);

  function updateTime() {
    const newTime = new Date().toLocaleTimeString();
    setTime(newTime);
  }

  useEffect(() => {
    if (arrayRandom) {
      handleShow();
    }
    return () => {};
  }, []);
  return (
    <div className="last-activity">
      <div className="last-activity-info">
        <div>
          <h3>Last Activity</h3>
          <p className="text-muted">January</p>
          <div className="activity-living">
            <div>
              <img
                className="living-image"
                src={LivingRoom}
                alt="living room"
              />
            </div>
            <div></div>
            <div className="activity-living-data">
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  <h3>Living Room</h3>
                  <p className="text-muted">People were detected</p>
                </div>
                <div className="text-muted">{time}</div>
              </div>
              <p
                className="text-muted"
                style={{ fontWeight: "lighter", fontSize: "10px" }}
              >
                Lorem ipsum dolor sit amet, consectetur and it adipiscing elit,
                sed do eiusmod tem incididunt ut labore et dolore magna enim ad
                minim veniam, quis nostrud. Lorem ipsum dolor sit amet,
                consectetur and it adipiscing elit, sed do eiusmod tem
                incididunt ut labore et dolore magna enim ad minim veniam, quis
                nostrud.
              </p>
            </div>
          </div>
          <div className="activity-living">
            <div>
              <img className="living-image" src={Bedroom} alt="living room" />
            </div>
            <div className="activity-living-data">
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  <h3>Living Room</h3>
                  <p className="text-muted">People were detected</p>
                </div>
                <div className="text-muted">{time}</div>
              </div>
              <p
                className="text-muted"
                style={{ fontWeight: "lighter", fontSize: "10px" }}
              >
                Lorem ipsum dolor sit amet, consectetur and it adipiscing elit,
                sed do eiusmod tem incididunt ut labore et dolore magna enim ad
                minim veniam, quis nostrud. Lorem ipsum dolor sit amet,
                consectetur and it adipiscing elit, sed do eiusmod tem
                incididunt ut labore et dolore magna enim ad minim veniam, quis
                nostrud.
              </p>
            </div>
          </div>
          <div className="activity-living">
            <div>
              <img className="living-image" src={Kitchen} alt="living room" />
            </div>
            <div className="activity-living-data">
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  <h3>Living Room</h3>
                  <p className="text-muted">People were detected</p>
                </div>
                <div className="text-muted">{time}</div>
              </div>
              <p
                className="text-muted"
                style={{ fontWeight: "lighter", fontSize: "10px" }}
              >
                Lorem ipsum dolor sit amet, consectetur and it adipiscing elit,
                sed do eiusmod tem incididunt ut labore et dolore magna enim ad
                minim veniam, quis nostrud. Lorem ipsum dolor sit amet,
                consectetur and it adipiscing elit, sed do eiusmod tem
                incididunt ut labore et dolore magna enim ad minim veniam, quis
                nostrud.
              </p>
            </div>
          </div>
          <div className="activity-living">
            <div>
              <img
                className="living-image"
                src={LivingRoom}
                alt="living room"
              />
            </div>
            <div className="activity-living-data">
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  <h3>Living Room</h3>
                  <p className="text-muted">People were detected</p>
                </div>
                <div className="text-muted">{time}</div>
              </div>
              <p
                className="text-muted"
                style={{ fontWeight: "lighter", fontSize: "10px" }}
              >
                Lorem ipsum dolor sit amet, consectetur and it adipiscing elit,
                sed do eiusmod tem incididunt ut labore et dolore magna enim ad
                minim veniam, quis nostrud. Lorem ipsum dolor sit amet,
                consectetur and it adipiscing elit, sed do eiusmod tem
                incididunt ut labore et dolore magna enim ad minim veniam, quis
                nostrud.
              </p>
            </div>
          </div>
          <div className="activity-living">
            <div>
              <img
                className="living-image"
                src={LivingRoom}
                alt="living room"
              />
            </div>
            <div className="activity-living-data">
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  <h3>Living Room</h3>
                  <p className="text-muted">People were detected</p>
                </div>
                <div className="text-muted">{time}</div>
              </div>
              <p
                className="text-muted"
                style={{ fontWeight: "lighter", fontSize: "10px" }}
              >
                Lorem ipsum dolor sit amet, consectetur and it adipiscing elit,
                sed do eiusmod tem incididunt ut labore et dolore magna enim ad
                minim veniam, quis nostrud. Lorem ipsum dolor sit amet,
                consectetur and it adipiscing elit, sed do eiusmod tem
                incididunt ut labore et dolore magna enim ad minim veniam, quis
                nostrud.
              </p>
            </div>
          </div>
          <div className="activity-living">
            <div>
              <img
                className="living-image"
                src={LivingRoom}
                alt="living room"
              />
            </div>
            <div className="activity-living-data">
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  <h3>Living Room</h3>
                  <p className="text-muted">People were detected</p>
                </div>
                <div className="text-muted">{time}</div>
              </div>
              <p
                className="text-muted"
                style={{ fontWeight: "lighter", fontSize: "10px" }}
              >
                Lorem ipsum dolor sit amet, consectetur and it adipiscing elit,
                sed do eiusmod tem incididunt ut labore et dolore magna enim ad
                minim veniam, quis nostrud. Lorem ipsum dolor sit amet,
                consectetur and it adipiscing elit, sed do eiusmod tem
                incididunt ut labore et dolore magna enim ad minim veniam, quis
                nostrud.
              </p>
            </div>
          </div>
          <div className="activity-living">
            <div>
              <img
                className="living-image"
                src={LivingRoom}
                alt="living room"
              />
            </div>
            <div className="activity-living-data">
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  <h3>Living Room</h3>
                  <p className="text-muted">People were detected</p>
                </div>
                <div className="text-muted">{time}</div>
              </div>
              <p
                className="text-muted"
                style={{ fontWeight: "lighter", fontSize: "10px" }}
              >
                Lorem ipsum dolor sit amet, consectetur and it adipiscing elit,
                sed do eiusmod tem incididunt ut labore et dolore magna enim ad
                minim veniam, quis nostrud. Lorem ipsum dolor sit amet,
                consectetur and it adipiscing elit, sed do eiusmod tem
                incididunt ut labore et dolore magna enim ad minim veniam, quis
                nostrud.
              </p>
            </div>
          </div>

          <div className="activity-living">
            <div>
              <img
                className="living-image"
                src={LivingRoom}
                alt="living room"
              />
            </div>
            <div className="activity-living-data">
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  <h3>Living Room</h3>
                  <p className="text-muted">People were detected</p>
                </div>
                <div className="text-muted">{time}</div>
              </div>
              <p
                className="text-muted"
                style={{ fontWeight: "lighter", fontSize: "10px" }}
              >
                Lorem ipsum dolor sit amet, consectetur and it adipiscing elit,
                sed do eiusmod tem incididunt ut labore et dolore magna enim ad
                minim veniam, quis nostrud. Lorem ipsum dolor sit amet,
                consectetur and it adipiscing elit, sed do eiusmod tem
                incididunt ut labore et dolore magna enim ad minim veniam, quis
                nostrud.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="last-activity-calender">
        <>
          <Calendar
            prev2Label={null}
            next2Label={null}
            onChange={setValue}
            value={value}
            showNeighboringMonth={false}
            defaultValue={new Date()}
          />
        </>
      </div>
      {console.log(value)}
      <Modal className="text-center" show={show} onHide={handleClose}>
        <Modal.Header className="activity-modal-header" closeButton>
          <div
            className="modal-div-content"
            style={{ marginTop: "10%", marginBottom: "5%" }}
          >
            <Modal.Title>Make call to 911</Modal.Title>
            <div>
              <p className="text-muted">
                Are you sure that you want to call the 911
                <br /> emergency service?
              </p>
            </div>
            <div className="modal-button">
              <Button
                className="acticity-modal-button"
                variant="light"
                onClick={handleClose}
              >
                Cancel
              </Button>
              &nbsp; &nbsp; &nbsp;
              <Button
                className="acticity-modal-button"
                variant="primary"
                onClick={handleClose}
              >
                Call
              </Button>
            </div>
          </div>
        </Modal.Header>
      </Modal>
    </div>
  );
}

export default LastActivity;
