import React, { useState } from "react";
import { Button, Modal, ModalBody, ModalFooter } from "react-bootstrap";
import newimage from "../../../assets/images/new image.png";
import "./devicescreen.css";

function DEvicescreen() {
  const [ShowModal, setShowModal] = useState();
  const [deviceName,setDeviceName]=useState();
  const handleShow = () => setShowModal(true);
  const handleClose = () => setShowModal(false);

  return (
    <div className="main-devicescreen-div">
      <p className="n-devices">
        <h3>New Device</h3>
      </p>
      <Button onClick={handleShow}>connect</Button>
      <div className="new-image-1">
        <img className="new-image" src={newimage} alt={Image}></img>
      </div>
      <div>
        <p className="p-camera">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam Lorem ipsum dolor sit amet, consectetur adipiscing elit,
          sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
      </div>

      <div className="u-devices">
        <p>{deviceName}</p>
      </div>
      <div>
        <Modal show={ShowModal} onHide={handleClose}>
          <Modal.Header closeButton></Modal.Header>
          <ModalBody className="modal-body1">
            <div classname="text-center ss">
              <h6>Device name</h6>
              <input type="text" onChange={(e)=>setDeviceName(e.target.value)}></input>
            </div>
            <Button className="save-button" onClick={()=>handleClose()}>save</Button>
          </ModalBody>
        </Modal>
      </div>
    </div>
  );
}

export default DEvicescreen;
