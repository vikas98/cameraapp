import { Button } from "@material-ui/core";
import { Android, Apple, Delete } from "@material-ui/icons";
import React from "react";
import PhoneImage from "../../../assets/images/subscriptionImage.png";
import "./subscriptionScreen.css";

function SubscriptionScreen() {
  return (
    <div className="subscription  text-center">
      <div className="subscription-header">
        <h3> Get Our Mobile Application</h3>
        <p>
          Lorem ipsum dolor sit amet, consectetur and it adipiscing elit, sed do
          eiusmod tem incididunt ut labore et dolore magna enim ad minim veniam,
          quis nostrud.
        </p>
      </div>
      <div className="subscription-links text-center">
        <Button variant="contained" color="secondary" startIcon={<Android />}>
          Get it on Google play
        </Button>
        &nbsp; &nbsp; &nbsp;
        <Button variant="contained" color="secondary" startIcon={<Apple />}>
          Get it on app store
        </Button>
      </div>
      <div className="text-center">
        <img
          className="subscription-image"
          src={PhoneImage}
          alt="imagaaaaa......"
        />
      </div>
    </div>
  );
}

export default SubscriptionScreen;
