import React, { useState } from "react";
import { Button, Col, Container, Modal, Row } from "react-bootstrap";
import "./viewAll.css";
import ReactPlayer from "react-player";
import Outside from "../../../assets/images/viewAllOutside.png";
import { CopyToClipboard } from "react-copy-to-clipboard";
import ButtonImage from "../../../assets/images/imageButton.png";
import {
  ChevronLeftOutlined,
  ChevronRightOutlined,
  Facebook,
  FullscreenOutlined,
  Message,
  PauseCircleFilledOutlined,
  PlayCircleFilledOutlined,
  Share,
  WhatsApp,
} from "@material-ui/icons";

function ViewAll() {
  const arrayRandom = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
  const [play, setPlay] = useState(false);
  const [show, setShow] = useState(false);
  const [copy, setCopy] = useState(false);
  const [email, setEmail] = useState("");
  const copyText =
    " https://www.google.com/search?q=mobile+sharing+browser+ui&rlz=1C5CHFA_enUA921UA921&sxsrf";

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const playFunction = (i) => {
    alert(i);
    setPlay(false);
  };
  const pauseFunction = (i) => {
    alert(i);
    setPlay(true);
  };
  return (
    <Container
      style={{ maxHeight: "100vh", overflowY: "scroll", overflowX: "hidden" }}
    >
      <div className="viewall-header">
        <h4 style={{ margin: "0" }}>View All</h4>
        <p style={{ fontSize: "12px", margin: "0" }} className="text-muted">
          11 active devices
        </p>
      </div>
      <Row>
        {arrayRandom.map((e, i) => {
          return (
            <Col className="viewall-col" sm={4}>
              {/* <img className="viewall-image" src={Outside} alt="ouside image" />
               */}
              <ReactPlayer
                muted={true}
                controls={true}
                width="400px"
                height="400px"
                url="https://www.youtube.com/watch?v=ysz5S6PUM-U"
              />
              <div className="button-image1">
                <Share onClick={handleShow} style={{ cursor: "pointer" }} />
                {/* <div>
                  <ChevronLeftOutlined />

                  <ChevronRightOutlined />
                </div>
                <FullscreenOutlined /> */}
              </div>
            </Col>
          );
        })}
      </Row>
      <Modal className="share-modal" show={show} onHide={handleClose}>
        <div className=" text-center share">
          <div className="share-main">
            <div>
              <h3>Share the Video</h3>
              <a href="#">{copyText} </a>
            </div>
            <div className="share-close-button">
              <Button
                style={{
                  borderRadius: "100%",
                  fontWeight: "bolder",
                  cursor: "pointer",
                }}
                variant="danger"
                onClick={handleClose}
              >
                X
              </Button>
            </div>
          </div>
          <div className="share-input">
            <div className="share-social-icons">
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-around",
                }}
              >
                <div>
                  <WhatsApp
                    style={{
                      minHeight: "50px",
                      minWidth: "50px",
                      color: "#25D366",
                      cursor: "pointer",
                    }}
                  />
                </div>
                <div>
                  <Facebook
                    style={{
                      minHeight: "50px",
                      minWidth: "50px",
                      color: " #4267B2",
                      cursor: "pointer",
                    }}
                  />
                </div>
                <div>
                  <Message
                    style={{
                      minHeight: "50px",
                      minWidth: "50px",
                      color: " rgb(202, 216, 8)",
                      cursor: "pointer",
                    }}
                  />
                </div>
              </div>
            </div>
            <p> or </p>
            <div style={{ display: "flex", justifyContent: "center" }}>
              <input
                type="text"
                placeholder="enter email address"
                className="input-tag-share"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              ></input>
              &nbsp; &nbsp;
              <Button variant="dark">Share Link</Button>
            </div>
            <div>
              <p> or </p>
              <CopyToClipboard
                text={copyText}
                onCopy={() => {
                  setCopy(true);
                  setTimeout(function () {
                    setCopy(false);
                  }, 3000);
                }}
              >
                <span
                  style={{
                    color: "blue",
                    textDecoration: "underline",
                    cursor: "pointer",
                  }}
                >
                  Copy Link
                </span>
              </CopyToClipboard>
              &nbsp; &nbsp; &nbsp;
              {copy && <spam style={{ color: "red" }}>Copied!</spam>}
            </div>
          </div>
        </div>
      </Modal>
    </Container>
  );
}

export default ViewAll;
