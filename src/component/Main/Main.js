import React from "react";
import Home from "../screens/home/Home";
import "./main.css";

function Main() {
  return (
    <>
      <Home />
    </>
  );
}

export default Main;
